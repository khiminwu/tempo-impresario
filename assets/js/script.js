var h=0;
var curr_page=0;
var total;
var scrollStat=0;
var counter;
$(document).ready(function(){

	total = $('.fullscreen').length;

	$(window).scroll(function() {
	  windowScroll();
	});

	$(window).resize(function() {
	  windowResize();
	});

	
	Page.init('#book1-booklet');
	Page.init('#book2-booklet');
	Page.init('#book3-booklet');
	$.each($('.about-item'),function(){
		$( this ).find('.about-inner').animate({opacity: 0}, {queue: false,duration: 0})
		
	})
	windowScroll();
	windowResize();
	
	if($(window).width()>640){
		pageScroll();
	}

	changeBook(0);
})

function wheelHandler(event){
	if($(window).width()>640){
		$('#main').unbind( "mousewheel");
		// console.log(event.deltaY);
	    if(event.deltaY<0){
	    	curr_page+=1;
	    }else if(event.deltaY>0){
	    	curr_page-=1;
	    }

	    if(curr_page<0){
	    	curr_page=0;
	    }

	    if(curr_page>=total-1){
	    	curr_page=total-1;
	    }
	     //console.log(curr_page);
	    pageScroll();
	}
}

function pageScroll(){
		var clip = $('.fullscreen')[curr_page];
		var top = 0-(curr_page * h);
		//console.log(top,$('#content-area'));
		scrollStat=1;
		$(clip).scrollTop(0);
		counter= setInterval(function(){
			windowScroll();
		},1);
		TweenMax.to($('#content-area'), 1, {top:top-10,onComplete:resetScroll});
}

function resetScroll(){
	setTimeout(function(){
		
		var clip = $('.fullscreen')[curr_page];
		if($(clip).prop('scrollHeight')<=h){
			$('#main').bind( "mousewheel", wheelHandler );
		}else{
			var last_scroll=0;
			var counter = 0;

			$(clip).bind( "mousewheel",function(event) {
				
				curr_scroll = event.deltaY;
				if(curr_scroll<0){
					curr_scroll=-1
				}else{
					curr_scroll=1;
				}


				if(curr_scroll!=last_scroll){
					counter=0;
				}

				if($(clip).scrollTop()+h>=$(clip).prop('scrollHeight') ){
					//event.deltaY<0
					counter+=1;
					if(counter>50){
						$(clip).unbind( "mousewheel");
						$('#main').bind( "mousewheel", wheelHandler );
					}
				}else if($(clip).scrollTop()==0){
					counter+=1;
					if(counter>50){
						$(clip).unbind( "mousewheel");
						$('#main').bind( "mousewheel", wheelHandler );
					}
				}
				

				last_scroll = curr_scroll;
			});

		}
		
		clearInterval(counter);
	},500)
}

function windowResize(){
	h = $(window).height();
	/*if(h<700){
		h=700;
	}*/

	$.each($('.fullscreen'),function(){
		if($(window).width()>640){
			$(this).css('height',h);
		}else{
			$(this).css('height','auto');
		}
	})
}

function windowScroll(){
	var top =0;
	if($(window).width()>640){
		top = Math.abs(parseInt($('#content-area').css('top')));
	}else{
		
		top = $(window).scrollTop();
	}
	//console.log(top)
	var top1 = (top*8)/100;
	var top2 = (top*12)/100;
	if(top1<0){
		top1 = 0;
	}
	top1 = 50 - top1;
	if(top2<0){
		top2 = 0;
	}
	top2 = 50 - top2;
	$('#diamond-area').css({'top':top1+'%'})
	$('#logo-area').css({'top':top2+'%'})

	$.each($('.about-item'),function(){
		var item_top = $(this).offset();
		var item_h = parseInt($(this).css('padding-top'))+50;
		item_top = (item_top.top-$(window).height())+item_h;
		if(item_top<top){
			 $( this ).find('.about-inner').animate({opacity: 1}, {queue: false,duration: 500})
		}
	})
	
}

function changeBook(count){
	$.each($('.bb-custom-wrapper'),function(key,val){
		if(key==count){
			$(val).removeClass('hide');
		}else{
			$(val).addClass('hide');
		}
	})

	$.each($('#book-list').find('a'),function(key,val){
		if(key==count){
			$(val).addClass('active');
		}else{
			$(val).removeClass('active');
		}
	});
}


var Page = (function() {
				
	var config,
		init = function(id) {
			var clip = $(id);
			
			config={
				$bookBlock : clip.find('.bb-bookblock'),
				$navNext : clip.find( '.bb-nav-next' ),
				$navPrev : clip.find( '.bb-nav-prev' ),
				$navFirst : clip.find( '.bb-nav-first' ),
				$navLast : clip.find( '.bb-nav-last' )
			}	
			config.$bookBlock.bookblock( {
				speed : 800,
				shadowSides : 0.8,
				shadowFlip : 0.7
			} );
			initEvents(config);
		},
		initEvents = function(config) {
			
			var $slides = config.$bookBlock.children();

			// add navigation events
			config.$navNext.on( 'click touchstart', function() {
				//console.log(this.config);
				config.$bookBlock.bookblock( 'next' );
				return false;
			} );

			config.$navPrev.on( 'click touchstart', function() {
				config.$bookBlock.bookblock( 'prev' );
				return false;
			} );

			config.$navFirst.on( 'click touchstart', function() {
				config.$bookBlock.bookblock( 'first' );
				return false;
			} );

			config.$navLast.on( 'click touchstart', function() {
				config.$bookBlock.bookblock( 'last' );
				return false;
			} );
			
			// add swipe events
			$slides.on( {
				'swipeleft' : function( event ) {
					config.$bookBlock.bookblock( 'next' );
					return false;
				},
				'swiperight' : function( event ) {
					config.$bookBlock.bookblock( 'prev' );
					return false;
				}
			} );

			// add keyboard events
			$( document ).keydown( function(e) {
				var keyCode = e.keyCode || e.which,
					arrow = {
						left : 37,
						up : 38,
						right : 39,
						down : 40
					};

				switch (keyCode) {
					case arrow.left:
						config.$bookBlock.bookblock( 'prev' );
						break;
					case arrow.right:
						config.$bookBlock.bookblock( 'next' );
						break;
				}
			} );
		};

		return { init : init };

})();